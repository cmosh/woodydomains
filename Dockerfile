FROM iron/base
WORKDIR /app
# copy binary into image
COPY hello /app/
ENTRYPOINT ["./hello"]